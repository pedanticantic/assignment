<html >
<head >
    <title >Linear Assignment Problem</title>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
    <script type="text/javascript" src="solve.js" ></script>
    <link href="solve.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body >
    <h1 >Linear Assignment Problem</h1>
    <h2 >Enter parameters</h2>
<?php

echo '<form method="post" name="linear_assignment" id="linear_assignment" >';

// Number of people.
echo '<p >Number of people <select name="num_people" id="num_people" >';
for( $i = 1 ; $i < 10 ; $i++ ) {
    $selected = ( 3 == $i ? 'selected="selected" ' : '' );
    echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
}
echo '</select></p>';

// CSL of tickets.
// 10,11,56,43,87,445,432,334,321,564
echo '<p >Comma-separated list of tickets <input type="text" name="tickets_csl" id="tickets_csl" size="60" value="" /></p>';

echo '<p ><input type="submit" name="solve" id="solve" value="Assign" /> Person goes green when the ticket has been assigned.</p>';

echo '</form>';

echo '<div id="solution" ></div>';

?>
</body>