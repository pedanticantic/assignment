$( document ).ready( function() {
    // Bind to the form submission. Prevent the default behaviour.
    $( '#linear_assignment' ).on( 'submit', function( event ) { event.preventDefault(); solve_problem(); })
});

// Some global variables.
// @TODO: Yes, this is naughty, and one day I'll do it better.
var $num_people = null;
var people = null;

function solve_problem() {
    // Make sure everything is in place.
    // We need the number of people, the CSL of tickets, and the div to put the results in.
    $num_people = $( '#num_people' ).val();
    $tickets_csl = $( '#tickets_csl' ).val();
    $result_container = $( '#solution' );
    if ( 0 < $num_people && 10 > $num_people && '' != $tickets_csl && null != $result_container ) {
        
        // Get the tickets into an array. We don't care about the format of the tickets themselves - it's not our problem.
        // Hmm, actually, it is our problem if they contain markup...
        $tickets_csl = $tickets_csl.replace( /&/g, "&amp;" ).replace( /</g, "&lt;" ).replace( />/g, "&gt;" );
        tickets = $tickets_csl.split( ',' );
        
        // Prepare the table of results.
        $results = '';
        people = new Array();
        for( tidx in tickets ) {
            // Each person cell needs an id. We remember the id, and other stuff in an array of objects.
            person_id = 'person_cell_' + tidx;
            people.push( { id: person_id, person: 0, interval: null, countdown: 0 } );
            $results += '<tr ><td >' + tickets[tidx] + '</td><td id="' + person_id + '" class="person_not_valid" ></td></tr>';
        }
        $results = '<table ><tr ><th >Ticket</th><th >Person</th></tr>' + $results + '</table>';
        $results += '<div id="ticket_count" ></div>';
        $( $result_container ).html( $results );
        
        // Now start off the "animation" that will put random people in the cells for a random time each.
        // When the animation ends, it makes sure the rules are satisfied. If not, it starts the animation again.
        for( pidx in people ) {
            start_interval( pidx );
        }

    } else {
        alert( 'There must be a number of people, a list of tickets and a solution area!' );
    }
}

// Method to start the "animation" for this ticket. It generates the number of iterations,
// stores it in the object, and initiates the interval.
function start_interval( index ) {
    console.log( 'start_interval: ' + index + '; id: ' + people[index].id );
    // Put between 10 and 20 numbers in the person cell before testing if they've valid.
    people[index].countdown = 10 + Math.floor( Math.random() * 10 );
    people[index].interval = window.setInterval( "handle_interval( " + index + " );", 150 );
}

// Handle this iteration.
// Put a random person in this cell. If the iteration has finished, see if the person is valid.
// If not, start the iteration again; otherwise finish (this ticket).
function handle_interval( index ) {
//console.log( 'handle_interval: ' + index + '; counter: ' + people[index].countdown );
    // Generate a random person. Remember it because not only will we put it on screen as
    // part of the animation, but we'll need it if the animation has finished.
    this_person = 1 + Math.floor( Math.random() * $num_people );
    $( '#' + people[index].id ).html( this_person );
    people[index].countdown--;
    if ( 0 > people[index].countdown ) {
        // The animation has finished. See if the number we generated above is valid.
        // We stop the animation, and restart it if necessary.
        clearInterval( people[index].interval );
        if ( is_valid( this_person ) ) {
            // It's valid. Make it official, and stop the animation. Mark the cell as "finished".
            people[index].person = this_person;
            $( '#' + people[index].id ).removeClass( 'person_not_valid' ).addClass( 'person_is_valid' );
        } else {
            // It's not valid. Restart the animation and try again.
            start_interval( index );
        }
    }
}

// Method to check whether assigning a ticket to the given person is valid or not.
function is_valid( person_number ) {
    // Count how many tickets everybody has. At the end, we need the minimum and maximum number of tickets
    // assigned to one person. These determine how many tickets the given person can have after assignment.
    // If this number is okay, we return true; otherwise false.
    // While we're doing this, we output the counts in the ticket_count div on screen.
    assigned = new Array();
    for( aidx = 0 ; aidx < $num_people ; aidx++ ) {
        assigned[1 + aidx] = 0;
    }
    for( pidx in people ) {
        curr_person = people[pidx].person;
        if ( 0 < curr_person ) {  // Zero means unassigned.
            assigned[curr_person]++;
        }
    }
    
    // Determine the min & max values.
    min_assigned = 999;
    max_assigned = 0;
    for( aidx in assigned ) {
        if ( min_assigned > assigned[aidx] ) {
            min_assigned = assigned[aidx];
        }
        if ( max_assigned < assigned[aidx] ) {
            max_assigned = assigned[aidx];
        }
    }
    
    // Now, if the min and max are different, the new count can be anywhere between the two, inclusive (in
    // practise, the difference will never be more than 1). If they are the same, the number count can either
    // be max or max-plus-one.
    new_count = 1 + assigned[person_number];
    console.log( assigned );
    console.log( 'Min: ' + min_assigned + '; Max: ' + max_assigned + '; New: ' + new_count );
    if ( min_assigned < max_assigned ) {
        result = ( new_count >= min_assigned && new_count <= max_assigned );
    } else {
        result = ( new_count == max_assigned || new_count == ( 1 + max_assigned ) );
    }
    console.log( 'New is: ' + ( result ? 'valid' : 'NOT valid' ) );
    
    // Get ready to output on screen.
    if ( result ) {
        assigned[person_number]++;  // Increment this person's number of tickets, if it's valid.
    }
    counts = '';
    for( aidx in assigned ) {
        // The line for this person on screen.
        counts += '<p >Person ' + aidx + ' has ' + assigned[aidx] + ' tickets.</p>';
    }
    $( '#ticket_count' ).html( counts );
    
    return result;
}
